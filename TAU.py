import time


class Tau:
    def __init__(self):
        self._list = []

    def Panel(self):
        while True:
            self._value = self.Value()

            if self._value.isdigit() == False:
                print('Please Enter a Number (Incorrect Value - {} )\n'.format(self._value))
                continue

            elif self._value.startswith('0') == True:
                print('Enter a Different Number From Zero (Incorrect Value - {} )\n'.format(self._value))
                continue

            else:
                self._value = int(self._value)
                self.Analyze()
                self._list = []
                continue

    def Value(self):
        return input('Enter a Number: ')

    def Analyze(self):
        [self._list.append(i) for i in range(1, (self._value + 1)) if self._value % i == 0]

        if self._value % len(self._list) == 0:
            print('{} - The Number is a TAU Number.'.format(self._value))

        else:
            print('{} - The Number is not a TAU Number.'.format(self._value))


if __name__ == '__main__':
    root = Tau()
    root.Panel()